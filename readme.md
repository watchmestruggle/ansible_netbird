# Prerequisites  
passwordless ssh access to the debian machine  

# Getting started  
1. set your target machine in `inventory.ini`
2. set your domain in `vars.yml`
3. run playbook with `ansible-playbook -i inventory.ini playbook.yml`

# what it does
installs netbird and zitadel on the target debian machine
fetches the credentials and stores them locally
